" leaders
let mapleader=" "

let s:curdir = fnamemodify(resolve(expand('<sfile>:p')), ':h')

" Load configs
for fpath in split(globpath(expand(s:curdir.'/fixes'), '*.vim'), '\n')
  exe 'source' fpath
endfor

" good display
set nowrap

" automatic indentation
set autoindent
set smartindent

" filetype detections
filetype on
filetype plugin on

if exists('g:vscode')
    nnoremap <leader>jw <Cmd>call VSCodeNotify('workbench.action.focusBelowGroup')<CR>
    nnoremap <leader>kw <Cmd>call VSCodeNotify('workbench.action.focusAboveGroup')<CR>
    nnoremap <leader>hw <Cmd>call VSCodeNotify('workbench.action.focusLeftGroup')<CR>
    nnoremap <leader>lw <Cmd>call VSCodeNotify('workbench.action.focusRightGroup')<CR>
    nnoremap <leader>nw <Cmd>call VSCodeNotify('workbench.action.focusNextGroup')<CR>

    function! s:saveAndClose() abort
        call VSCodeCall('workbench.action.files.save')
        call VSCodeNotify('workbench.action.closeActiveEditor')
    endfunction

    nnoremap <Leader>q <Cmd>call VSCodeNotify('workbench.action.closeActiveEditor')<CR>
    " nnoremap <Leader>w <Cmd>call <SID>saveAndClose()<CR>
    nnoremap <Leader>w <Cmd>call VSCodeCall('workbench.action.files.save')<CR>
else
    " fast close
    nnoremap <Leader>q :q<cr>
    nnoremap <Leader>w :w<cr>
endif

if !exists('g:vscode')

    " window operations
    nnoremap <Leader>nw <C-W><C-W>
    nnoremap <Leader>lw <C-W>l
    nnoremap <Leader>hw <C-W>h
    nnoremap <Leader>jw <C-W>j
    nnoremap <Leader>kw <C-W>k

    " swap windows
    function! MarkWindowSwap()
            let g:markedWinNum = winnr()
    endfunction


    function! DoWindowSwap()
        "Mark destination
        let curNum = winnr()
        let curBuf = bufnr( "%" )
        exe g:markedWinNum . "wincmd w"
        "Switch to source and shuffle dest->source
        let markedBuf = bufnr( "%" )
        "Hide and open so that we aren't prompted and keep history
        exe 'hide buf' curBuf
        "Switch to dest and shuffle source->dest
        exe curNum . "wincmd w"
        "Hide and open so that we aren't prompted and keep history
        exe 'hide buf' markedBuf 
    endfunction

    nmap <silent> <leader>mw :call MarkWindowSwap()<CR>
    nmap <silent> <leader>sw :call DoWindowSwap()<CR>
endif

" searches
set incsearch
set smartcase
set nocompatible
set wildmenu

" plugins
call plug#begin('~/.config/nvim-plugins')
if !exists('g:vscode')
    " rainbow parenthesis
    Plug 'luochen1990/rainbow'
    Plug 'altercation/vim-colors-solarized'
    " show git status
    Plug 'tpope/vim-fugitive'
    " file browser
    Plug 'Shougo/defx.nvim', { 'do': ':UpdateRemotePlugins' }
    " coc
    Plug 'neoclide/coc.nvim', {'branch': 'release'}
    " commenter
    Plug 'tpope/vim-commentary'
    " indentation display
    Plug 'Yggdroot/indentLine'
    " pretty status line
    Plug 'itchyny/lightline.vim'
    " fish shell syntax
    Plug 'vim-scripts/fish-syntax'
    " buf explorer
    Plug 'fholgado/minibufexpl.vim'
    " highlight support for coc-clangd
    Plug 'jackguo380/vim-lsp-cxx-highlight'
endif
call plug#end()

" colors
let g:solarized_termcolors=16
if has("unix")
  let s:uname = system("uname")
  if s:uname == "Darwin\n"
    " Do Mac stuff here
    let g:solarized_termcolors=16
  endif
endif
let g:solarized_termtrans=1
set background=dark
colorscheme solarized

" always status
set laststatus=2
set noshowmode
set ruler
set number
if !exists('g:vscode')
    set cursorline
    set cursorcolumn
endif

" syntax
syntax enable
syntax on

" indentation
filetype indent on
set expandtab
set tabstop=4
set shiftwidth=4
set softtabstop=4

" search highlights
set hlsearch
nnoremap <Leader>hl :noh<CR>

let g:rainbow_active=1
let g:indentLine_char='|'

let g:lightline = {
      \ 'colorscheme': 'wombat',
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ],
      \             [ 'gitbranch' ],
      \             [ 'readonly', 'filename', 'modified'] ],
      \   'right': [ [ 'lineinfo' ],
      \              [ 'percent' ],
      \              [ 'cocstatus', 'fileformat', 'fileencoding', 'filetype'] ]
      \ },
      \ 'component_function': {
      \   'gitbranch': 'FugitiveHead',
      \   'cocstatus': 'COCStatusDiagnostic'
      \ },
      \ }

if exists('g:vscode')
    xmap gc  <Plug>VSCodeCommentary
    nmap gc  <Plug>VSCodeCommentary
    omap gc  <Plug>VSCodeCommentary
    nmap gcc <Plug>VSCodeCommentaryLine
endif

if !exists('g:vscode')
    nnoremap <Leader>fl :Defx<CR>
    " settings in defx
    call defx#custom#column('filename', {
          \ 'min_width': 40,
          \ 'max_width': 40,
          \ })
    call defx#custom#option('_', {
          \ 'winwidth': 35,
          \ 'columns': 'mark:indent:icon:filename:type',
          \ 'split': 'vertical',
          \ 'direction': 'topleft',
          \ 'show_ignored_files': 0,
          \ 'buffer_name': '',
          \ 'toggle': 1,
          \ 'resume': 1
          \ })
    " Keymap in defx
    autocmd FileType defx call s:defx_my_settings()
    function! s:defx_my_settings() abort
        setl nospell
        setl signcolumn=no
        setl nonumber
        nnoremap <silent><buffer><expr> <CR>
            \ defx#is_directory() ?
            \ defx#do_action('open_or_close_tree') :
            \ defx#do_action('drop',)
        nnoremap <silent><buffer><expr> C defx#do_action('copy')
        nnoremap <silent><buffer><expr> P defx#do_action('paste')
        nnoremap <silent><buffer><expr> R defx#do_action('rename')
        nnoremap <silent><buffer><expr> D defx#do_action('remove')
        nnoremap <silent><buffer><expr> N defx#do_action('new_multiple_files')
        nnoremap <silent><buffer><expr> - defx#do_action('drop', 'split')
        nnoremap <silent><buffer><expr> \ defx#do_action('drop', 'vsplit')
        nnoremap <silent><buffer><expr> T defx#do_action('drop', 'tabe')
        nnoremap <silent><buffer><expr> r defx#do_action('redraw')
    endfunction

    " coc.nvim
    set hidden
    set nobackup
    set nowritebackup
    set cmdheight=2
    set updatetime=250
    set shortmess+=c
    set signcolumn=yes
    " tab for completion select
    inoremap <silent><expr> <TAB>
          \ pumvisible() ? "\<C-n>" :
          \ <SID>check_back_space() ? "\<TAB>" :
          \ coc#refresh()

    inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

    function! s:check_back_space() abort
      let col = col('.') - 1
        return !col || getline('.')[col - 1]  =~# '\s'
    endfunction
    " Highlight the symbol and its references when holding the cursor.
    autocmd CursorHold * silent call <SID>show_documentation()
    " nmap <leader>hi :call CocAction('doHover')<cr>
    " Remap keys for applying codeAction to the current line.
    nmap <leader>ca  <Plug>(coc-codeaction)
    " Apply AutoFix to problem on the current line.
    nmap <leader>cf  <Plug>(coc-fix-current)
    " force lightline update
    autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

    function! s:show_documentation()
        call CocActionAsync('doHover')
    endfunction

    " coc status function
    function! COCStatusDiagnostic() abort
      let info = get(b:, 'coc_diagnostic_info', {})
      if empty(info) | return 'NoCoc' | endif
      let msgs = []
      if get(info, 'error', 0)
        call add(msgs, 'E' . info['error'])
      endif
      if get(info, 'warning', 0)
        call add(msgs, 'W' . info['warning'])
      endif
      return join(msgs, ' ') . ' ' . get(g:, 'coc_status', '')
    endfunction

    " MBE Confs
    let g:miniBufExplAutoStart=1
    let g:miniBufExplBuffersNeeded=0

    " coc-go
    autocmd BufWritePre *.go :call CocAction('runCommand', 'editor.action.organizeImport')
endif

if !exists('g:vscode')
    " coclist shortcuts
    nnoremap <silent> <leader>lf :<C-u>CocList files<cr>
    nnoremap <silent> <leader>lc :<C-u>CocList commands<cr>
    nnoremap <silent> <leader>ls :<C-u>CocList -I symbols<cr>
    nnoremap <silent> <leader>lm :<C-u>CocList maps<cr>
    nnoremap <silent> <leader>lb :<C-u>CocList buffers<cr>
    nnoremap <silent> <leader>ll :<C-u>CocListResume<cr>
    " coc gotos
    nmap <silent> <leader>gd <Plug>(coc-definition)
    nmap <silent> <leader>gt <Plug>(coc-type-definition)
    nmap <silent> <leader>gi <Plug>(coc-implementation)
    nmap <silent> <leader>gr <Plug>(coc-references)
else
    " list shortcuts
    nnoremap <silent> <leader>lf <Cmd>call VSCodeCall('workbench.action.quickOpen')<CR>
    nnoremap <silent> <leader>lc <Cmd>call VSCodeCall('workbench.action.showCommands')<CR>
    nnoremap <silent> <leader>ls <Cmd>call VSCodeCall('workbench.action.showAllSymbols')<CR>
    " gotos
    nmap <silent> <leader>gd <Cmd>call VSCodeCall('editor.action.revealDefinition')<CR>
    nmap <silent> <leader>gt <Cmd>call VSCodeCall('editor.action.goToTypeDefinition')<CR>
    nmap <silent> <leader>gi <Cmd>call VSCodeCall('editor.action.goToImplementation')<CR>
    nmap <silent> <leader>gr <Cmd>call VSCodeCall('editor.action.goToReferences')<CR>
    " peeks
    nmap <silent> <leader>pd <Cmd>call VSCodeCall('editor.action.peekDefinition')<CR>
    nmap <silent> <leader>pt <Cmd>call VSCodeCall('editor.action.peekTypeDefinition')<CR>
    nmap <silent> <leader>pi <Cmd>call VSCodeCall('editor.action.peekImplementation')<CR>
    nmap <silent> <leader>pr <Cmd>call VSCodeCall('editor.action.referenceSearch.trigger')<CR>
endif
