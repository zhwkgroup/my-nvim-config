# nvim-configuration

My neovim configuration & plugins.

## COC EXTENSIONS

coc-clangd

coc-go

coc-highlight

coc-markdownlint

coc-calc

coc-json

coc-pairs

coc-lists

